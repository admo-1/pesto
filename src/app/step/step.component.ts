import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css']
})
export class StepComponent implements OnInit {
  currentStep: number;
  stepDescs: string[];


  constructor(private _Activatedroute: ActivatedRoute) {
    this.currentStep = +this._Activatedroute.snapshot.paramMap.get('id');
  }

  ngOnInit() {

    this.stepDescs = [
      '',
      'Faites cuire les pâtes al dente. ',
      'Dans un mortier, pilez le basilic avec l\'ail et les pignons jusqu\'à obtention d\'une crème lisse. ',
      'Mettez la préparation dans un bol, puis ajouter l\'huile peu à peu en fouettant. ',
      'Rajoutez les fromages râpés, assaisonner et mélangez. ',
      'Egouttez les pâtes, mélangez-les au pesto et servez. ',
    ];
  }

  forward() {
    if (this.currentStep < this.stepDescs.length) {
      this.currentStep++;
    }
  }

  backward() {
    if (0 < this.currentStep) {
      this.currentStep--;
    }
  }

}
